<?php

$config['default_controller'] = 'home';
$config['assets_dir'] = 'assets/';


// Your classes should be in the app/classes directory, here
// you can set the order in which they're loaded
$config['classes'] = array();


// Templates for the different error pages
$config['errors']['default'] = 'error.html';
$config['errors']['not_found'] = '404.html';



// This is a regex of characters that SHOULD NOT be allowed
// in a URL
$config['invalid_url_chars'] = '/&/';


// Database settings. If there's not database for this project
// just leave this empty
/* 
    $config['database']['default']['db_name'] = '';
    $config['database']['default']['host'] = '';
    $config['database']['default']['username'] = '';
    $config['database']['default']['password'] = '';
*/




// -------------- ROUTES --------------
// Here you can set up customized URLs, pretty much
// just like codeigniter
// $config['routes']['[regex here]'] = '[result here]';


// Making this an object because I want to
$config = json_decode(json_encode($config));
