<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
    <html class='no-js' lang='en'>
<!--<![endif]-->

    <head>
        <meta charset='utf-8' />
        <meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible' />
        <meta content='width=device-width, initial-scale=1.0' name='viewport' />

        <title>{if isset($page_title)}{$page_title} | {/if}MVSimple</title>
        <meta name="description" content="">
        <meta name="keywords" content="">

        <link rel="stylesheet" type="text/css" href="{$site->asset_url()}css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="{$site->asset_url()}css/custom.css" />
        <link rel="stylesheet" type="text/css" href="{$site->asset_url()}css/main.css" />

    </head>
    <body {if isset($body_id)}id="{$body_id}"{/if}>
        <div class="container">
            <div class="row-fluid">
                <div class="span5">
                    <h2><a href="{$site->base_url()}">OVC</a> <small>Because models are for suckers</small></h2>
                </div>
                <div class="span7">
                    {include file="includes/menu.tpl"}
                </div>
            </div>
