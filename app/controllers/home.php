<?php

class Home extends Controller{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $view_data = array();

        $view_data['page_title'] = "Home";
        $view_data['body_id'] = 'page-home-index';
        $view_data['curr'] = 'home';

        $this->view('includes/header', $view_data);
        $this->view('home/index', $view_data);
        $this->view('includes/footer', $view_data);
    }
}
