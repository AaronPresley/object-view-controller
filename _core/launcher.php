<?php

class Launcher{
    // Public properties
    public $segments;
    public $config;

    // Private properties
    

    public function __construct($do_run = FALSE){
        $this->get_config();
        $this->segments = $this->parse_query_string($_SERVER['QUERY_STRING']);

        $this->include_classes();
        $this->determine_db_status();

        // We know our stuff, now run the controllers
        if ( $do_run ) { $this->run_controller(); }
    }

    // -------------- PRIVATE FUNCTIONS --------------
    private function include_classes(){
        if ( !file_exists(PATH.'/app/classes/') )
            throw new Exception("You are missing an app/classes directory");

        // Loading our list classes first
        foreach( $this->config->classes as $class ){
            $file = PATH.'/app/classes/'.$class;
            if ( !file_exists($file) )
                throw new Exception("The class '$class' doesn't exist in the app/classes/ directory");
            include_once($file);
        }
    }

    private function determine_db_status(){
        if ( isset($this->config->database) ){
            // Test if PDO exists on this system
            if ( !defined('PDO::ATTR_DRIVER_NAME') )
                throw new Exception("You have entered DB information, but PDO cannot be detected on your system");
        }
    }

    private function run_controller(){
        $controller = strtolower(current($this->segments));

        // If no controllers are provided, go to default controller
        if ( $controller == "" )
            $controller = $this->config->default_controller;

        // See if a controller of that name exists
        if ( !file_exists(PATH.'/app/controllers/'.$controller.'.php') )
            header("Location: /". $this->config->errors->not_found);

        require_once(PATH.'/app/controllers/'.$controller.'.php');

        // Instanciating the controller
        $obj = new $controller();

        // Calling the function
        $func = isset($this->segments[1]) && !$this->segments[1] == "" ? $this->segments[1] : 'index';
        $obj->$func();
    }

    private function get_config(){
        if ( !file_exists(PATH.'/config.php') )
            throw new Exception("A config file must exist in ". PATH ."/_core/");
        require(PATH.'/config.php');

        if ( !isset($config) || !is_object($config) )
            throw new Exception("Your config file is incorrect");

        $this->config = $config;
        return TRUE;
    }

    private function parse_query_string($string){
        // Making sure there aren't any bad characters
        if ( preg_match($this->config->invalid_url_chars, $string) )
            header("Location: /".$this->config->errors->default);

        // Looping through our routes to see if there's a match
        if ( isset($this->config->routes) ){
            foreach( $this->config->routes as $pattern => $url ){
                if( preg_match($pattern, $string) ){
                    $string = preg_replace($pattern, $url, $string);
                    break;
                }
            }
        }


        // String was valid, explode it
        return explode('/', ltrim($string, '/'));
    }
    
}

$site = new Launcher(TRUE);
