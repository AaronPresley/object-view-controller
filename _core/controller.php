<?php

class Controller{
    public $input;
    public $segments;
    public $config;

    public function __construct(){
        $this->gen_input();

        $launcher = new Launcher();
        $this->segments = $launcher->segments;
        $this->config = $launcher->config;
    }

    public function view($file = NULL, $vars = array()){
        if ( !$file )
            throw new Exception("You didn't tell us which template file to show.");
        if ( !is_array($vars) )
            throw new Exception("The second parameter must be an array");

        // Append .tpl to the file if it wasn't included
        if ( !preg_match('/\.tpl$/', $file) )
            $file .= '.tpl';

        // Setting up Smarty
        $smarty = new Smarty();
        $smarty->setTemplateDir(PATH.'/app/views/');
        $smarty->setCompileDir(PATH.'/_core/SmartyTemplates/views_c/');

        // Assigning our variables
        foreach( $vars as $var => $val )
            $smarty->assign($var, $val);

        $common = new Common();
        $smarty->assign('site', $common);
        $smarty->assign('environment', ENVIRONMENT);

        $smarty->display($file);
    }

    // -------------- PRIVATE FUNCTIONS --------------
    private function gen_input(){
        $this->input = new StdClass();

        $this->input->post = array();
        if ( count($_POST) ){
            foreach( $_POST as $field => $post_item ){
                $post_item = trim(htmlspecialchars($post_item));
                $post_item == "" ? NULL : $post_item;

                $this->input->post[$field] = $post_item;
            }
        }
    }
}
