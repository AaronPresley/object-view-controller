<?php

class Common{
    public $config;

    public function __construct(){
        $launcher = new Launcher();
        $this->config = $launcher->config;
        $this->segments = $launcher->segments;
    }

    public function asset_url(){
        return $this->base_url() . $this->config->assets_dir;
    }

    public function base_url(){
        $protocol = strtolower(current(explode('/', $_SERVER['SERVER_PROTOCOL']))) . '://';
        $base_url = $protocol . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'];
        return preg_replace('/index\.php$/', '', $base_url);
    }
}
